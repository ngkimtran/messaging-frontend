/* eslint-disable no-undef */
describe("Messaging app", function () {
  beforeEach(function () {
    cy.visit("http://localhost:3000");
  });

  it("Log in form is shown", function () {
    cy.contains("Log In");
    cy.contains("Create a New Account");
    cy.contains("Forgot Password?");
  });

  describe("Login", function () {
    it("succeeds with correct credentials", function () {
      cy.get("#username").type("admin");
      cy.get("#password").type("sekret");
      cy.get(".login-btn").click();
      cy.contains("Conversations");
    });
  });

  describe("When logged in", function () {
    beforeEach(function () {
      cy.get("#username").type("admin");
      cy.get("#password").type("sekret");
      cy.get(".login-btn").click();
    });

    describe("and a conversation exists", function () {
      it("User can click into the conversation", function () {
        cy.contains("tester").click();
        cy.contains("Hello from swagger");
        cy.contains("this is a message from tester");
      });

      it("User can send a new message", function () {
        cy.contains("admin").click();
        cy.get(".chat-message-input").type("Hello from cypress");
        cy.contains("Send").click();
        cy.contains("Hello from cypress");
      });
    });

    it("User can logout", function () {
      cy.get(".avatar-img").click();
      cy.contains("Logout").click();
      cy.contains("Log In");
      cy.contains("Create a New Account");
      cy.contains("Forgot Password?");
    });
  });
});
