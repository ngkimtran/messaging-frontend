# MESSAGING REST API

Messaging frontend created with Create React App.

## Environment variables

| Name                  | Description                             |
| --------------------- | --------------------------------------- |
| REACT_APP_BACKEND_URL | The backend url for database connection |
| CLOUD_URL             | Cloud url for image upload              |

## Pre-requisites

Install [Node.js](https://nodejs.org/en/)

## Getting started

- Clone the repository

```
git clone  git@gitlab.com:ngkimtran/messaging-frontend.git messaging-frontend
```

- Install dependencies

```
cd messaging-frontend
npm install

```

- Run tests

```

  npm test

```

- Build and run the project

```

npm start

```

Navigate to `http://localhost:3000`

## Project Structure

The folder structure of this app is explained below:

| Name               | Description                                                                                      |
| ------------------ | ------------------------------------------------------------------------------------------------ |
| **src**            | All the source code of the application are stored here.                                          |
| **src/components** | Small reuseable components seperated.                                                            |
| **src/pages**      | Landing routes for the application                                                               |
| **src/contexts**   | Contexts handler                                                                                 |
| **services**       | Services for handling the backend                                                                |
| **src/utils**      | Helper functions queries                                                                         |
| **cypress**        | Test folder files                                                                                |
| App.js             | Main app                                                                                         |
| index.js           | Root element configuration app                                                                   |
| package.json       | Contains npm dependencies as well as [build scripts](#what-if-a-library-isnt-on-definitelytyped) |

## Testing

The tests are done using cypress

```

 "cypress": "^9.6.0",

```

## Contributors

- Kim Tran - 1900309
- Mai Tran - 1900308

## License

[MIT](https://choosealicense.com/licenses/mit/)

```

```

