import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import service from "../../services/users";
import config from "../../utils/config";
import "./register.css";

const Register = () => {
  const navigate = useNavigate();
  const username = useRef();
  const email = useRef();
  const name = useRef();
  const password = useRef();
  const rePassword = useRef();
  const [file, setFile] = useState(null);

  const uploadImage = async () => {
    if (!file) return null;
    const imgData = new FormData();
    imgData.append("file", file);
    imgData.append("upload_preset", "uploads");

    //upload avatar to cloudinary

    const res = await fetch(config.CLOUD_URL, {
      method: "POST",
      body: imgData,
    });

    const data = await res.text();
    return JSON.parse(data);
  };

  const onRegister = async (e) => {
    e.preventDefault();
    if (rePassword.current.value !== password.current.value) {
      rePassword.current.setCustomValidity("Passwords don't match!");
    }

    try {
      if (file) {
        const img = await uploadImage();

        await service.register({
          username: username.current.value,
          name: name.current.value,
          email: email.current.value,
          password: password.current.value,
          avatar: img.url,
        });
      } else {
        await service.register({
          username: username.current.value,
          name: name.current.value,
          email: email.current.value,
          password: password.current.value,
        });
      }

      navigate("/login");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <div className="register">
        <div className="register-wrapper">
          <div className="register-left">
            <h3 className="register-logo">Messaging App</h3>
            <span className="register-desc">
              Connect with friends and start sending messages
            </span>
          </div>
          <div className="register-right">
            <form className="register-form" onSubmit={onRegister}>
              <div className="register-input">
                <div className="avatar-wrapper">
                  <label className="avatar-label">Profile picture</label>
                  <input
                    type="file"
                    className="register-avatar"
                    onChange={({ target }) => setFile(target.files[0])}
                  />
                </div>
              </div>
              <input
                placeholder="Name"
                type="text"
                className="register-input"
                required
                ref={name}
                autoComplete="on"
              />
              <input
                placeholder="Email"
                type="email"
                className="register-input"
                required
                ref={email}
                autoComplete="on"
              />
              <input
                placeholder="Username"
                type="text"
                className="register-input"
                required
                ref={username}
                autoComplete="on"
              />
              <input
                placeholder="Password"
                type="password"
                className="register-input"
                required
                ref={password}
                autoComplete="on"
              />
              <input
                placeholder="Password Again"
                type="password"
                className="register-input"
                required
                ref={rePassword}
                autoComplete="on"
              />
              <button className="register-btn" type="submit">
                Sign Up
              </button>
              <button
                className="register-loginbtn"
                onClick={() => navigate("/login")}
              >
                Login to your Account
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
