import { useContext, useState, useEffect, useRef } from "react";
import { io } from "socket.io-client";

import Contact from "../../components/contact/Contact";
import AddNewContactModal from "../../components/contact/AddNewContactModal";
import Conversation from "../../components/conversation/Conversation";
import Message from "../../components/message/Message";
import { StateContext } from "../../contexts/state";
import messageService from "../../services/messages";
import { fetchMessages, addNewMessage } from "../../contexts/actions";

import "./messenger.css";
import Header from "../../components/header/Header";
import config from "../../utils/config";

const Messenger = () => {
  const [{ user, conversations, contactList, messages }, dispatch] =
    useContext(StateContext);
  const [currentChat, setCurrentChat] = useState(null);
  const [newMessage, setNewMessage] = useState("");
  const scrollRef = useRef();
  const [close, setClose] = useState(true);

  //socket variables
  const socket = useRef();
  const [arrivalMessage, setArrivalMessage] = useState("");

  useEffect(() => {
    socket.current = io(config.BACKEND_URL);
    socket.current.on("getMessage", (data) => {
      setArrivalMessage({
        senderId: data.senderId,
        content: data.content,
        createdAt: Date.now(),
      });
    });
  }, []);

  useEffect(() => {
    socket.current.emit("newUser", user.id);
    socket.current.on("getUsers", (users) => {
      console.log(users);
    });
  }, [user]);

  useEffect(() => {
    const newMessages = async () => {
      if (
        arrivalMessage &&
        currentChat?.members.includes(arrivalMessage.senderId)
      ) {
        dispatch(addNewMessage(arrivalMessage));
      }
    };
    newMessages();
  }, [currentChat, arrivalMessage]);

  useEffect(() => {
    const initializeMessages = async () => {
      if (currentChat !== null) {
        const messages = await messageService.getMessages(currentChat?.id);
        dispatch(fetchMessages(messages));
      }
    };
    initializeMessages();
  }, [currentChat, dispatch]);

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const message = {
      content: newMessage,
      conversationId: currentChat.id,
    };

    const receiverId = currentChat.members.find((m) => m !== user.id);
    socket.current.emit("sendMessage", {
      content: newMessage,
      senderId: user.id,
      receiverId,
    });

    await messageService.createMessage(message);
    const messages = await messageService.getMessages(currentChat?.id);
    dispatch(fetchMessages(messages));
    setNewMessage("");
  };

  return (
    <>
      <Header />
      <div className="messenger">
        <div className="chat-menu">
          <div className="chat-menu-wrapper">
            <span className="chat-menu-title">Conversations</span>
            {conversations?.map((c) => (
              <div key={c.id} onClick={() => setCurrentChat(c)}>
                <Conversation item={c} currentUser={user} />
              </div>
            ))}
          </div>
        </div>
        <div className="chat-box">
          <div className="chat-box-wrapper">
            {currentChat ? (
              <>
                <div className="chat-box-top">
                  {messages?.map((m, i) => (
                    <div key={i} ref={scrollRef}>
                      <Message
                        item={m}
                        currentUser={user}
                        currentChat={currentChat}
                        own={m.senderId.id === user.id}
                      />
                    </div>
                  ))}
                </div>
                <div className="chat-box-bottom">
                  <textarea
                    className="chat-message-input"
                    placeholder="Write to your friend!"
                    onChange={(e) => setNewMessage(e.target.value)}
                    value={newMessage}
                  ></textarea>
                  <button className="chat-submit-button" onClick={handleSubmit}>
                    Send
                  </button>
                </div>
              </>
            ) : (
              <span className="no-conv-text">
                Open a conversation to start chatting.
              </span>
            )}
          </div>
        </div>
        <div className="chat-contact">
          <div className="chat-contact-wrapper">
            {contactList?.map((c) => (
              <Contact key={c.id} contact={c} setCurrentChat={setCurrentChat} />
            ))}
            <div className="contact-modal-wrapper">
              <button
                className="add-contact-button"
                onClick={() => setClose(false)}
              >
                Add new Contact
              </button>
            </div>
          </div>
        </div>
        {!close && <AddNewContactModal setClose={setClose} />}
      </div>
    </>
  );
};

export default Messenger;

