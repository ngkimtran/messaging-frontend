import React, { useContext, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { StateContext } from "../../contexts/state";
import { loginFailure, loginStart, loginSuccess } from "../../contexts/actions";
import loginService from "../../services/login";
import "./login.css";

const Login = () => {
  const username = useRef();
  const password = useRef();
  const navigate = useNavigate();
  const [{ isFetching }, dispatch] = useContext(StateContext);

  const onLogin = async (e) => {
    e.preventDefault();

    //set isFetching to true
    dispatch(loginStart());

    try {
      //login from the server
      const newUser = await loginService.login({
        username: username.current.value,
        password: password.current.value,
      });

      //set user in store
      dispatch(loginSuccess(newUser));
      navigate("/");
    } catch (error) {
      //set error
      loginFailure();
    }
  };

  return (
    <div className="login">
      <div className="login-wrapper">
        <div className="login-left">
          <h3 className="login-logo">Messaging App</h3>
          <span className="login-desc">
            Connect with friends and start sending messages
          </span>
        </div>
        <div className="login-right">
          <form className="login-form" onSubmit={onLogin}>
            <input
              placeholder="Username"
              type="text"
              className="login-input"
              id="username"
              required
              ref={username}
              autoComplete="on"
            />
            <input
              placeholder="Password"
              type="password"
              className="login-input"
              id="password"
              required
              ref={password}
              autoComplete="on"
            />
            <button className="login-btn" type="submit" disabled={isFetching}>
              {isFetching ? (
                <CircularProgress color="inherit" size="20px" />
              ) : (
                "Log In"
              )}
            </button>
            <button
              className="login-register"
              onClick={() => navigate("/register")}
            >
              {isFetching ? (
                <CircularProgress color="inherit" size="20px" />
              ) : (
                "Create a New Account"
              )}
            </button>
            <span className="login-forgot">Forgot Password?</span>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
