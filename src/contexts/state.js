import { createContext, useReducer } from "react";
import storage from "../utils/storage";
import reducer from "./reducer";

export const INITIAL_STATE = {
  user: storage.loadUser() || null,
  isFetching: false,
  error: false,
  contactList: [],
  conversations: [],
  messages: [],
};

export const StateContext = createContext(INITIAL_STATE);

export const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <StateContext.Provider value={[state, dispatch]}>
      {children}
    </StateContext.Provider>
  );
};
