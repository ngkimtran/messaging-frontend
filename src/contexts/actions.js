import storage from "../utils/storage";

export const loginStart = () => {
  return { type: "LOGIN_START" };
};

export const loginSuccess = (user) => {
  storage.setUser(user);
  return { type: "LOGIN_SUCCESS", payload: user };
};

export const logout = () => {
  storage.removeUser();
  return { type: "LOGOUT" };
};

export const loginFailure = () => {
  return { type: "LOGIN_FAILURE" };
};

export const addContact = (contact) => {
  return { type: "ADD_CONTACT", payload: contact };
};

export const fetchContacts = (contacts) => {
  return { type: "FETCH_CONTACTS", payload: contacts };
};

export const fetchConversations = (conversations) => {
  return { type: "FETCH_CONVERSATIONS", payload: conversations };
};

export const fetchMessages = (messages) => {
  return { type: "FETCH_MESSAGES", payload: messages };
};

export const addConversation = (conversation) => {
  return { type: "NEW_CONVERSATION", payload: conversation };
};

export const addNewMessage = (message) => {
  return { type: "NEW_MESSAGE", payload: message };
};
