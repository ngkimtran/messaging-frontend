import { INITIAL_STATE } from "./state";

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN_START":
      return {
        user: null,
        isFetching: true,
        error: false,
      };
    case "LOGIN_SUCCESS":
      return {
        user: action.payload,
        isFetching: false,
        error: false,
      };
    case "LOGIN_FAILURE":
      return {
        user: null,
        isFetching: false,
        error: true,
      };
    case "LOGOUT":
      return {
        ...INITIAL_STATE,
      };
    case "ADD_CONTACT":
      return {
        ...state,
        user: {
          ...state.user,
          contactList: [...state.contactList, action.payload],
        },
      };
    case "FETCH_CONTACTS":
      return {
        ...state,
        contactList: [...action.payload],
      };

    case "FETCH_CONVERSATIONS":
      return {
        ...state,
        conversations: [...action.payload],
      };
    case "FETCH_MESSAGES":
      return {
        ...state,
        messages: action.payload,
      };
    case "NEW_CONVERSATION":
      return {
        ...state,
        conversations: [...state.conversations, action.payload],
      };
    case "NEW_MESSAGE":
      return {
        ...state,
        messages: [...state.messages, action.payload],
      };
    default:
      return state;
  }
};

export default reducer;
