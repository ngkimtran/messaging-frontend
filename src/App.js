import { Routes, Route, Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";

import conversationService from "./services/conversations";
import usersService from "./services/users";

import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import Messenger from "./pages/messenger/Messenger";

import { fetchConversations, fetchContacts } from "./contexts/actions";
import { StateContext } from "./contexts/state";

const App = () => {
  const [{ user }, dispatch] = useContext(StateContext);
  useEffect(() => {
    const initializeConversations = async () => {
      const conversations = await conversationService.getConversations();
      dispatch(fetchConversations(conversations));
    };
    const initializeContacts = async () => {
      const userInfo = await usersService.getSingleUser(user.id);
      dispatch(fetchContacts(userInfo.contactList));
    };

    if (user !== null) {
      initializeConversations();
      initializeContacts();
    }
  }, [dispatch, user]);

  return (
    <Routes>
      <Route
        path="/register"
        element={user ? <Navigate replace to="/" /> : <Register />}
      />
      <Route
        path="/login"
        element={user ? <Navigate replace to="/" /> : <Login />}
      />
      <Route
        path="/"
        element={user ? <Messenger /> : <Navigate replace to="/login" />}
      />
      <Route path="*" element={<Navigate to="/" replace />} />
    </Routes>
  );
};

export default App;

