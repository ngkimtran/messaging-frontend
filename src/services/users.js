import axios from "axios";
import utils from "../utils/config";
import storage from "../utils/storage";

const baseURL = utils.BACKEND_URL + "/api/users";

const register = async (user) => {
  const res = await axios.post(baseURL, user);
  return res.data;
};

const getUsers = async () => {
  const res = await axios.get(baseURL);
  return res.data;
};

const getSingleUser = async (id) => {
  const res = await axios.get(`${baseURL}/${id}`);
  return res.data;
};

const getToken = () => storage.loadUser()?.token;

const addContact = async (contactId) => {
  const res = await axios.put(`${baseURL}/contacts`, contactId, {
    headers: { Authorization: `bearer ${getToken()}` },
  });
  return res.data;
};

const updateUser = async (user) => {
  const res = await axios.put(`${baseURL}`, user, {
    headers: { Authorization: `bearer ${getToken()}` },
  });
  return res.data;
};

const findUsersByName = async (name) => {
  const res = await axios.get(`${baseURL}?name=${name}`);
  return res.data;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  register,
  getUsers,
  getSingleUser,
  addContact,
  updateUser,
  findUsersByName,
};
