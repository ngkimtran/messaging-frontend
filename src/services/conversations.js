import axios from "axios";
import utils from "../utils/config";
import storage from "../utils/storage";

const getToken = () => storage.loadUser()?.token;
const baseURL = utils.BACKEND_URL + "/api/conversations";

const getConversations = async () => {
  const config = {
    headers: { Authorization: `bearer ${getToken()}` },
  };
  const res = await axios.get(baseURL, config);
  return res.data;
};

const createConversation = async (conversation) => {
  const config = {
    headers: { Authorization: `bearer ${getToken()}` },
  };
  const res = await axios.post(baseURL, conversation, config);
  return res.data;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  getConversations,
  createConversation,
};
