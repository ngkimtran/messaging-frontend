import axios from "axios";
import utils from "../utils/config";

const baseURL = utils.BACKEND_URL + "/api/login";

const login = async (user) => {
  const res = await axios.post(baseURL, user);
  return res.data;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default { login };
