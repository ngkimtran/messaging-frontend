import axios from "axios";
import utils from "../utils/config";
import storage from "../utils/storage";

const getToken = () => storage.loadUser().token;
const baseURL = utils.BACKEND_URL + "/api/messages";

const getMessages = async (conversationId) => {
  const config = {
    headers: { Authorization: `bearer ${getToken()}` },
  };
  const res = await axios.get(`${baseURL}/${conversationId}`, config);
  return res.data;
};

const createMessage = async (conversation) => {
  const config = {
    headers: { Authorization: `bearer ${getToken()}` },
  };
  const res = await axios.post(baseURL, conversation, config);
  return res.data;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  getMessages,
  createMessage,
};
