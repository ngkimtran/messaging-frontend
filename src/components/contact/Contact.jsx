import { useContext } from "react";
import { addConversation } from "../../contexts/actions";
import { StateContext } from "../../contexts/state";
import conversationsService from "../../services/conversations";
import "./contact.css";

const Contact = ({ contact, setCurrentChat }) => {
  const [{ user, conversations }, dispatch] = useContext(StateContext);
  const members = [user.id, contact.id];

  const openConversation = async () => {
    const match = conversations.find((c) => {
      let newMembers = [...members];
      c.members.forEach((o) => {
        newMembers = newMembers.filter((n) => n !== o);
      });
      if (newMembers.length === 0) {
        return c;
      } else return null;
    });

    if (!match) {
      const newConversation = await conversationsService.createConversation({
        receiver: contact.id,
      });
      dispatch(addConversation(newConversation));
      setCurrentChat(newConversation);
    } else {
      setCurrentChat(match);
    }
  };

  return (
    <div className="contact" onClick={() => openConversation()}>
      <div className="contact-item">
        <img
          src={
            contact.avatar
              ? contact.avatar
              : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png?fbclid=IwAR3HZiOi-BprIkjbZOJKttjyGC3sA0cx8WdsBPAq6_FWeb7XWAVNjhoUhM0"
          }
          alt=""
          className="contact-item-img"
        />
        <div className="contact-item-name">{contact.name}</div>
      </div>
    </div>
  );
};

export default Contact;

