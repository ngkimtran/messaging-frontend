import React, { useContext, useEffect, useState } from "react";
import "./contact-modal.css";
import { useDebounce } from "use-debounce";

import usersService from "../../services/users";
import Contact from "./Contact";

import { StateContext } from "../../contexts/state";
import { addContact } from "../../contexts/actions";

import SearchIcon from "@mui/icons-material/Search";
import AddIcon from "@mui/icons-material/Add";

const AddNewContactModal = ({ setClose }) => {
  const [name, setName] = useState("");
  const [searchValue] = useDebounce(name, 500);
  const [searchResult, setSearchResult] = useState([]);
  const [{ user }, dispatch] = useContext(StateContext);

  useEffect(() => {
    const search = async () => {
      if (searchValue !== "") {
        let data = await usersService.findUsersByName(searchValue);
        data = data?.filter(
          (d) => d.name.toLowerCase() !== user.name.toLowerCase()
        );

        setSearchResult(data);
      } else setSearchResult([]);
    };
    search();
  }, [searchValue]);

  const newContact = async (contact) => {
    await usersService.addContact({ contactId: contact.id });
    dispatch(
      addContact({
        id: contact.id,
        name: contact.name,
        username: contact.username,
        avatar: contact.avatar,
      })
    );
  };

  return (
    <div className="new-contact-modal">
      <div className="modal-wrapper">
        <span className="modal-close" onClick={() => setClose(true)}>
          X
        </span>
        <h3>Find new friends and connect with them!</h3>
        <span className="search-wrapper">
          <SearchIcon />
          <input
            placeholder="Search for contacts..."
            className="search-input"
            onChange={({ target }) => setName(target.value)}
          />
        </span>
        {searchResult.length > 0 ? (
          <>
            {searchResult.map((c) => (
              <div key={c.id} className="result-wrapper">
                <Contact contact={c} />
                <AddIcon className="add-btn" onClick={() => newContact(c)} />
              </div>
            ))}
          </>
        ) : (
          <span>No result found.</span>
        )}
      </div>
    </div>
  );
};

export default AddNewContactModal;
