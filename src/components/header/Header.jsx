import React, { useContext, useState, useRef, useEffect } from "react";
import { StateContext } from "../../contexts/state";
import RightIcon from "@mui/icons-material/ChevronRight";
import { useNavigate } from "react-router-dom";
import "./header.css";
import { logout } from "../../contexts/actions";
import { CSSTransition } from "react-transition-group";
import Form from "./Form";

const Header = () => {
  const [{ user }, dispatch] = useContext(StateContext);
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();

  const onLogout = () => {
    dispatch(logout());
    navigate("/login");
  };

  return (
    <nav className="navbar">
      <ul className="navbar-nav">
        <h3 className="app-logo">Messaging App</h3>
        <img
          src={
            user.avatar
              ? user.avatar
              : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png?fbclid=IwAR3HZiOi-BprIkjbZOJKttjyGC3sA0cx8WdsBPAq6_FWeb7XWAVNjhoUhM0"
          }
          alt=""
          className="avatar-img"
          onClick={() => setOpen(!open)}
        />
        {open && <DropDownMenu onLogout={onLogout} />}
      </ul>
    </nav>
  );
};

const DropDownMenu = ({ onLogout }) => {
  const [activeMenu, setActiveMenu] = useState("main");
  const [menuHeight, setMenuHeight] = useState(null);
  const dropdownRef = useRef(null);

  useEffect(() => {
    setMenuHeight(dropdownRef.current?.firstChild.offsetHeight);
  }, []);

  const calcHeight = (el) => {
    const height = el.offsetHeight;
    setMenuHeight(height);
  };

  const DropDownItem = ({ children, onClick }) => {
    return (
      <div className="menu-item" onClick={onClick}>
        {children} <RightIcon />
      </div>
    );
  };

  return (
    <div className="dropdown" style={{ height: menuHeight }} ref={dropdownRef}>
      <CSSTransition
        in={activeMenu === "main"}
        timeout={500}
        classNames="menu-primary"
        unmountOnExit
        onEnter={calcHeight}
      >
        <div className="menu">
          <DropDownItem key="profile" onClick={() => setActiveMenu("profile")}>
            Update profile
          </DropDownItem>
          <DropDownItem key="logout" onClick={onLogout}>
            Logout
          </DropDownItem>
        </div>
      </CSSTransition>
      <CSSTransition
        in={activeMenu === "profile"}
        timeout={500}
        classNames="menu-secondary"
        unmountOnExit
        onEnter={calcHeight}
      >
        <Form onClick={() => setActiveMenu("main")} />
      </CSSTransition>
    </div>
  );
};

export default Header;
