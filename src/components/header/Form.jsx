import React, { useRef, useState } from "react";
import "./header.css";
import LeftIcon from "@mui/icons-material/ChevronLeft";
import usersService from "../../services/users";
import config from "../../utils/config";

const Form = ({ onClick }) => {
  const name = useRef("");
  const [file, setFile] = useState(null);

  const onSubmit = async () => {
    let userToUpdate;
    if (name?.current.value !== "") {
      userToUpdate = {
        name: name.current.value,
      };
    }

    if (file) {
      const imgData = new FormData();
      imgData.append("file", file);
      imgData.append("upload_preset", "uploads");

      //upload avatar to cloudinary

      const res = await fetch(config.CLOUD_URL, {
        method: "POST",
        body: imgData,
      });

      const data = await res.text();
      const img = JSON.parse(data);

      userToUpdate = {
        avatar: img.url,
      };
    }

    const updatedUser = await usersService.updateUser(userToUpdate);
    console.log(updatedUser);
  };

  return (
    <div className="menu" style={{ height: 160 }}>
      <LeftIcon onClick={onClick} />
      <input className="input" placeholder="Name" ref={name} />
      <div className="input">
        <label className="picture-label">Profile picture</label>
        <input
          type="file"
          className="picture"
          onChange={({ target }) => setFile(target.files[0])}
        />
      </div>
      <button className="profile-update-submit-btn" onClick={() => onSubmit()}>
        Update
      </button>
    </div>
  );
};

export default Form;
