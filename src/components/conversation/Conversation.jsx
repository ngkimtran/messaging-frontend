import "./conversation.css";
import userService from "../../services/users";
import { useEffect, useState } from "react";

const Conversation = ({ item, currentUser }) => {
  const [friend, setFriend] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      if (item.members?.every((m) => m === currentUser.id)) {
        const data = await userService.getSingleUser(currentUser.id);
        setFriend(data);
      } else {
        const data = await userService.getSingleUser(
          item.members.find((m) => m !== currentUser.id)
        );
        setFriend(data);
      }
    };
    getUser();
  }, [currentUser, item]);

  return (
    <div className="conversation">
      <img
        src={
          friend?.avatar
            ? friend.avatar
            : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png"
        }
        alt=""
        className="conversation-img"
      />
      <span className="conversation-name">{friend?.name}</span>
    </div>
  );
};

export default Conversation;

