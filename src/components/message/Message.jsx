import "./message.css";
import { useState, useEffect } from "react";
import { format } from "timeago.js";
import userService from "../../services/users";

const Message = ({ item, currentUser, currentChat, own }) => {
  const [friend, setFriend] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      if (currentChat.members.every((m) => m === currentUser.id)) {
        const data = await userService.getSingleUser(currentUser.id);
        setFriend(data);
      } else {
        const data = await userService.getSingleUser(
          currentChat.members.find((m) => m !== currentUser.id)
        );
        setFriend(data);
      }
    };
    getUser();
  }, [currentChat.members, currentUser, item]);

  if (own) {
    return (
      <div className="message own">
        <div className="message-top">
          <p className="message-text">{item.content}</p>
          <img
            src={
              currentUser?.avatar
                ? currentUser.avatar
                : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png"
            }
            alt=""
            className="message-img"
          />
        </div>
        <div className="message-bottom">{format(item.createdAt)}</div>
      </div>
    );
  }

  return (
    <div className="message">
      <div className="message-top">
        <img
          src={
            friend?.avatar
              ? friend.avatar
              : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png"
          }
          alt=""
          className="message-img"
        />
        <p className="message-text">{item.content}</p>
      </div>
      <div className="message-bottom">{format(item.createdAt)}</div>
    </div>
  );
};

export default Message;
