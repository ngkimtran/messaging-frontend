const setUser = (user) =>
  window.localStorage.setItem("msgapp:token", JSON.stringify(user));

const loadUser = () => {
  const rawUser = window.localStorage.getItem("msgapp:token");
  return rawUser ? JSON.parse(rawUser) : null;
};

const removeUser = () => window.localStorage.removeItem("msgapp:token");

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  setUser,
  loadUser,
  removeUser,
};
